/*
 * Circle.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */
#include "Circle.h"

namespace Student1 {

	Circle::Circle(double r) {
		setR(r);
	}

	Circle::~Circle() {
	}

	void Circle::setR(double r) {
		this->r = r;
	}

	double Circle::getR() {
		return r;
	}

	double Circle::calculateCircumference() {
		return PI * r * r;
	}

	double Circle::calculateArea() {
		return PI * r * 2;
	}
	bool Circle::equality(Circle& circle4, Circle& circle5){
		
		if (circle4.getR() == circle5.getR())
			return true;
		else if(circle4.getR() == circle5.getR())
			return false;
		
	}
	
}