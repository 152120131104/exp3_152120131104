/*
 * Triangle.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */

#include "Triangle.h"

Triangle::Triangle(double a, double b, double c) {
	setA(a);
	setB(b);
	setC(c);
}

Triangle::~Triangle() {

}

void Triangle::setA(double a) {
	a = a;
}

void Triangle::setB(double b) {
	b = b;
}

void Triangle::setC(double c) {
	c = c;
}

double Triangle::calculateCircumference() {
	return a + b + c ;
}
