/*
 * Circle.h
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */
#ifndef CIRCLE_H_
#define CIRCLE_H_
namespace Student1
{
	class Circle {
	public:
		Circle(double);
		virtual ~Circle();
		void setR(double);
		double getR() const;
		double calculateCircumference() const;
		double calculateArea();
	private:
		double r;
		const double PI = 3.14;
	};
}
#endif /* CIRCLE_H_ */

